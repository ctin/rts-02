	var timeToLoadOS = 250;
	var timeToLoad = 43;

	var fileFromServer = "";
	var mainObject = {};
	var changedFileFromServer = "";
	var changedMainObject = {};
	var properties = [];
	
	function testIP(text) {
		return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(text);
	}
	function uploadCfg(onFinish) {
		if(typeof getPlacedVariables === "function")
			getPlacedVariables();
		setExtractedVariables(function() {
			processRequest({
				'id' : 'saveConfigButton',
				'uri' : 'uploadCfg',
				'messageAbout' : 'Загрузка конфигурации на сервер.',
				'requestType'	:	'POST',
				'messageToSend':	changedFileFromServer,
				'callback' : function(responseText) {
					if(typeof clearAllBackgrounds === "function")
						clearAllBackgrounds();
					for(var pos = 0;  pos < properties.length;  pos++)
						mainObject[properties[pos]] = changedMainObject[properties[pos]];
					if(typeof onFinish !== "function") {
						uploadNTPconf(refreshPageVariables);
					}
					else
						onFinish();
				}
			})
		});
	}
	function extractVariables() {
		var jsonStr = ["{"];
		var lines = fileFromServer.split("\n");
		for (var j = 0;  j < lines.length;  j++) {
			if (lines[j].match(/\s*=\s*/)) {
				var prop = lines[j].split(/\s*=\s*/)[0];
				var val = lines[j].split(/\s*=\s*/)[1];
				if(val) {
					if(val.indexOf("[") !== -1 && val.indexOf("]") !== -1) {
						mainObject[prop] = val.match(/\[(.*?)\]/)[1].replace(/\s*,\s*/g, ',').split(',');
					} else {
						mainObject[prop] = val;
					}
				}
			}
			
			if(lines[j].match(/\".*\"\s*:\s*{/)) {
				for (var subStrPos = j; subStrPos < lines.length;  subStrPos++) {
					jsonStr.push(lines[subStrPos]);
					if(lines[subStrPos].match(/}/)) {
						jsonStr.push(",");
						break;
					}
				}					
			}				
		}
		if(jsonStr[jsonStr.length - 1] == ",")
			jsonStr.pop();
		jsonStr.push("}");
		var templates = JSON.parse(jsonStr.join("\n"));
		for(var tmp in templates) 
			mainObject[tmp] = templates[tmp];
	}
	
	function padStr(i) {
		return (i < 10) ? "0" + i : "" + i;
	}
	function printDate(dateTime, printDate, printTime) {
		var dateStr = "";
		if(printDate) {
			dateStr += padStr(dateTime.getFullYear()) + "-" + 
				padStr(1 + dateTime.getMonth()) + "-" + 
				padStr(dateTime.getDate());
		}
		if(printDate && printTime)
			dateStr += " ";
		if(printTime) {
			dateStr += padStr(dateTime.getHours()) + ":" + 
				padStr(dateTime.getMinutes()) + ":" +
				padStr(dateTime.getSeconds());
		}
					  
		return dateStr;
	}
		
	function setExtractedVariables(onFinish) {
		processRequest({
			'uri' : 'getVersion',
			'messageAbout' : 'Загрузка информации об ОС.',
			'checkContentcheckContent' : function(responseText) {
				return responseText.indexOf('Linux release') !== -1;
			},
			'callback' : function(responseText) {
				var lines = responseText.split('\n');
				var line = lines[0];
				var prefix = 'Linux release';
				var str = line.slice(line.indexOf(prefix) + prefix.length);
				var version = str.match(/.*#\d+/);
				var dateStr = str.match(/#\d+.*/)[0];
				dateStr = dateStr.replace(/#\d+/, '');
				dateStr = dateStr.replace('MSK', '');
				var dateOS = new Date(dateStr);
				var envProperties=["updated", "date", "time", "version_OS", "date_OS"];

				changedMainObject.updated = 1;
				changedMainObject.date = printDate(new Date(), 1, 0);
				changedMainObject.time = printDate(new Date(), 0, 1);
				changedMainObject.version_OS = version;
				changedMainObject.date_OS = printDate(dateOS, 1, 1);
				for(var pos = 0;  pos < properties.length;  pos++) {
					var reg = new RegExp(properties[pos] + "\\s*=.*", 'm');
					var strToAppend;
					if(!changedMainObject[properties[pos]])
						strToAppend = "";				
					else if(Object.prototype.toString.call( changedMainObject[properties[pos]]) === '[object Array]')
						strToAppend = "[" + changedMainObject[properties[pos]] + "]";
					else
						strToAppend = changedMainObject[properties[pos]];
					strToAppend = properties[pos] + " = " + strToAppend;
					if(changedFileFromServer.match(reg))
						changedFileFromServer = changedFileFromServer.replace(reg, strToAppend);
					else
						changedFileFromServer = changedFileFromServer.replace('_end_of_file_', strToAppend + '\n_end_of_file_');
				}
				
				for(var pos = 0;  pos < envProperties.length;  pos++) {
					var reg = new RegExp(envProperties[pos] + "\\s*=.*", 'm');
					var strToAppend = envProperties[pos] + " = " + (changedMainObject[envProperties[pos]] ? changedMainObject[envProperties[pos]] : "");
					if(changedFileFromServer.match(reg))
						changedFileFromServer = changedFileFromServer.replace(reg, strToAppend);
					else
						changedFileFromServer = changedFileFromServer.replace('_end_of_file_', strToAppend + '\n_end_of_file_');
				}
				if(typeof onFinish === "function")
					onFinish();
			}
		});
		$('body').css("cursor", "auto");
		$('.server-button').attr('disabled', false);
	}
	
	function getCurrentCfg(onFinish) {
		processRequest({
			'id' : 'loadConfigButton',
			'uri' : 'getCfg',
			'messageAbout' : 'Загрузка конфигуации с сервера.',
			'checkContent' : function(responseText) {
				return responseText.indexOf('end_of_file') !== -1;
			},
			'callback' : function(responseText) {
				fileFromServer = responseText;
				changedFileFromServer = fileFromServer;
				extractVariables();
				for(var pos = 0;  pos < properties.length;  pos++) {
					changedMainObject[properties[pos]] = mainObject[properties[pos]];
				}
				if(typeof placeVariables === "function")
					placeVariables();
				if(typeof checkAll === "function")
					checkAll();
				if(typeof onFinish === "function")
					onFinish();
			}
		});
	}
	var refreshPageVariables = getCurrentCfg;
	var counterServerCfg = 0;
	var intervalIDServerCfg;
	function addSecToBarServerCfg() {
		var progress = Math.floor((counterServerCfg++ / timeToLoad) * 100);
		$bar = $('.bar');
		$bar.css(
			'width',
			progress + '%'
		);
		$bar.text(progress + "%");
		if(counterServerCfg == timeToLoad) {
			clearInterval(intervalIDServerCfg);
			$('#progress').hide();
			$('#myModal').modal('hide');
			location.reload();
		}
	}
	
	function reboot(arg) {
		if(typeof arg === "undefined")
			arg = 0;
		$("#myModalLabel").html("Происходит перезагрузка сервера");
		$('#myModal').modal('show');
		if(arg === 1)
			counterServerCfg = 0;
		intervalIDServerCfg = setInterval(addSecToBarServerCfg, 1000);
		addSecToBarServerCfg();
		processRequest({
			'uri' : 'reboot',
			'messageAbout' : 'Перезагрузка сервера.',
			'requestType'	:	'POST',
			'messageToSend':	arg,
			'needRefreshAfterBadStatus' : true
		});
	}
	
	function saveAndRebootFunction(onFinish) {
		changedMainObject.updated = "0";
		uploadCfg(onFinish);		
	}
	function changeNetworkSettings(onFinish) {
		if(!testIP(mainObject.ipaddr) || !testIP(mainObject.netmask) || !testIP(mainObject.gatewayip) || !testIP(mainObject.serverip)) {
			alert("Сетевые настройки введены некорректно и сохранены в память прибора не будут!");
			if(typeof onFinish === "function")
				onFinish();
			return;
		}
		processRequest({
			'uri' : 'changeNetworkSettings',
			'messageAbout' : 'Установка сетевых параметров.',
			'requestType'	:	'POST',
			'messageToSend':	mainObject.ipaddr + "%%" + mainObject.netmask + "%%" + mainObject.gatewayip + "%%" + mainObject.serverip,
			'callback' : function(responseText) {
				if(typeof onFinish === "function")
					onFinish();
			}
		});
	}
	function createResolvConf(onFinish) {
		var text = "";
		text += '# Our domain\n';
		if(typeof mainObject.domain !== "undefined" && mainObject.domain.length) {
			text += 'domain ' + mainObject.domain + '\n';
		}
		var validDNS1 = (typeof mainObject.DNS_server1 !== "undefined" && testIP(mainObject.DNS_server1));
		var validDNS2 = (typeof mainObject.DNS_server2 !== "undefined" && testIP(mainObject.DNS_server2));
		if(validDNS1 || validDNS2)
		{
			if(validDNS1) {
				text += 'nameserver ' + mainObject.DNS_server1 + '\n';
			}
			if(validDNS2) {
				text += 'nameserver ' + mainObject.DNS_server2 + '\n';
			}
		}
		uploadResolvConf(text, onFinish);
	}
	function uploadResolvConf(text, onFinish) {
		processRequest({
			'uri' : 'uploadResolvConf',
			'messageAbout' : 'Загрузка на сервер файла resolv.conf .',
			'requestType'	:	'POST',
			'messageToSend':	text,
			'callback' : function(responseText) {
				if(typeof onFinish === "function")
					onFinish();
			}
		});
	}

	function createInetdConf(onFinish) {
		processRequest({
			'uri' : 'getInetdConf',
			'messageAbout' : 'Скачивание файла inet.d с сервера .',
			'callback' : function(text) {
				var opts = ['telnet', 'ftp', 'ssh'];
				for(var i = 0;  i < opts.length;  i++) {
					var opt = opts[i];
					if(typeof mainObject['use_' + opt] !== "undefined" && mainObject['use_' + opt].length) {
						var str = opt + ' ' + mainObject[opt + '_command'];
						if(mainObject['use_' + opt] === "0") {
							str = "# " + str;
						}
						var reg = new RegExp('^\\s*[*#]*\\s*' + opt + '.*', 'm');
						if(text.match(reg)) {
							text = text.replace(reg, str);
						} else {
							if(text[text.length - 1] !== '\n')
								text += '\n';
							text += str + '\n';
						}
					}
				}
				uploadInetdConf(text, onFinish);
			}
		});
	}
	function uploadInetdConf(text, onFinish) {
		processRequest({
			'uri' : 'uploadInetdConf',
			'messageAbout' : 'Загрузка на сервер файла inet.d .',
			'requestType'	:	'POST',
			'messageToSend':	text,
			'callback' : function(responseText) {
				if(typeof onFinish === "function")
					onFinish();
			}
		});
	}
	function createUserrcSh(onFinish) {
		processRequest({
			'uri' : 'getUserrcSh',
			'messageAbout' : 'Скачивание файла user_rc.sh с сервера .',
			'checkContent' : function(text) {
				var valid = text.indexOf('\0') === -1
					&& !!text.match(/^\s*hostname.*/m)
					&& !!text.match(/^\s*echo Setup UART1.*/m)
					&& !!text.match(/^\s*stty -F \/dev\/ttyBF1.*/m)
					&& !!text.match(/^\s*[*#]*\s*appweb.*/m);
				return valid;
			},
			'callback' : function(text) {
				if(typeof mainObject.hostname !== "undefined" && mainObject.hostname.length) {
					text = text.replace(/^\s*hostname.*/m, 'hostname ' + mainObject.hostname);
				}
				if(typeof mainObject.protocol !== "undefined" && mainObject.protocol !== "NONE") {
					var str = 'echo Setup UART1 for ' + mainObject.refid + ' "(' + mainObject.baudrate + ' baud ' + mainObject.databits + mainObject.parity
						+ mainObject.stopbits + ')"';
					text = text.replace(/^\s*echo Setup UART1.*/m, str);
					str = 'stty -F /dev/ttyBF1 ' + mainObject.baudrate + ' cs' + mainObject.databits + ' ';
					if(mainObject.parity === 'O') {
						str += 'parodd parenb ';
					} else if(mainObject.parity === 'N') {
						str += '-parodd -parenb ';
					}
					if(mainObject.stopbits === "1") {
						str += '-cstopb ';
					} else if (mainObject.stopbits === "2") {
						str += 'cstopb ';
					}
					text = text.replace(/^\s*stty -F \/dev\/ttyBF1.*/m, str);
				}
				str = mainObject.http_command;
				if(typeof mainObject.use_http !== "undefined" && mainObject.use_http.length) {
					if(mainObject.use_http === "0") {
						str = "# " + str;
					}
					text = text.replace(/^\s*[*#]*\s*appweb.*/m, str);
				}
				uploadUserrcSh(text, onFinish);
			}
		});	
	}
	
	function uploadUserrcSh(text, onFinish) {
		processRequest({
			'uri' : 'uploadUserrcSh',
			'messageAbout' : 'Загрузка на сервер файла user_rc.sh .',
			'requestType'	:	'POST',
			'messageToSend':	text,
			'callback' : function(responseText) {
				if(typeof onFinish === "function")
					onFinish();
			}
		});
	}
	var callbacks = [getCurrentCfg, changeNetworkSettings, createUserrcSh, createInetdConf, createResolvConf,
		saveAndRebootFunction, uploadNTPconf, reboot];
	function processCallback(index) {
		if(index >= (callbacks.length))
			return;
		addSecToBarServerCfg();
		var nextCallback;
		if(index + 1 < callbacks.length) {
			nextCallback = function() {
				processCallback(index + 1)
			}
		}	
		callbacks[index](nextCallback);
	}
	function saveAndReboot() {
		$("#myModalLabel").html("Происходит сохранение настроек");
		$('#myModal').modal('show');
		counterServerCfg = 0;
		if(typeof getPlacedVariables === "function") {
			getPlacedVariables();
			for(var pos = 0;  pos < properties.length;  pos++)
				mainObject[properties[pos]] = changedMainObject[properties[pos]];
		}
		processCallback(refreshPageVariables === getCurrentCfg ? 1 : 0);
	}