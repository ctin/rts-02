function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

function checkRequest(options) {
	var default_args = {
		'responseText'	:	"",
		'id'	:	"",
		'message':	'',
		'checkContent': undefined,
		'callback' : undefined
	}
	for(var index in default_args) {
		if(typeof options[index] === "undefined") options[index] = default_args[index];
	}

	var errMsg = "";
	var displayMsg = function() {
		if(options.id && document.querySelector('#' + options.id) && typeof showPopoverMessage === "function") {
			showPopoverMessage(options.id, errMsg, 5);
		} else {
			alert(options.message.toString() + ' ' + errMsg);
		}
	}
	if(options.responseText.indexOf("end") !== -1) {
		options.responseText = options.responseText.replace(/[\n]?end[\n]*$/, '');
		if(typeof options.checkContent === "function") {
			if(options.checkContent(options.responseText)) {
				// ok, go down to callback
			} else {
				errMsg = "Данные с сервера переданы некорректно!";
				displayMsg();
				return 2;
			}
		}
		if(typeof options.callback === "function") {
			options.callback(options.responseText);
		}	
		return 0;
	} else if (options.responseText.indexOf("error") !== -1) {
		if(options.responseText.indexOf('null request size') !== -1) { 
			errMsg = "Отсутствует сообщение от клиента!";
		} else if(options.responseText.indexOf('command') !== -1) {
			errMsg = "Серверу была отправлена некорректная комманда!";
		} else if(options.responseText.indexOf('no such file') !== -1) {
			errMsg = "Искомый файл отсутствует на сервере";
		} else if(options.responseText.indexOf('no such dir') !== -1) {
			errMsg = "Искомая директория отсутствует на сервере";
		} else if(options.responseText.indexOf('login') !== -1) {
			errMsg = "Некорректный логин!";
		} else if(options.responseText.indexOf('user already exists') !== -1) {
			errMsg = "Такой пользователь уже есть в системе!";
		} else if(options.responseText.indexOf('password') !== -1) {
			errMsg = "Некорректный пароль!";
		} else if(options.responseText.indexOf('group') !== -1) {
			errMsg = "Некорректная группа!";
		} else if(options.responseText.indexOf('file was not copied') !== -1) {
			errMsg = "Файл не был скопирован!";
		} else {
			errMsg = options.responseText.match(/.*error.*/)[0]
		}
	} else {
		errMsg = "Программа на сервере не выполнена!";
	}
	displayMsg();
	return 1;
}

function processRequest(options) {
	var default_args = {
		'uri'	:	"",
		'requestType'	:	'GET',
		'id'	:	"",
		'messageToSend':	null,
		'messageAbout': '',
		'checkContent': undefined,
		'callback' : undefined,
		'onLoad' : undefined,
		'needRefreshAfterBadStatus' : false
	}
	for(var index in default_args) {
		if(typeof options[index] === "undefined") options[index] = default_args[index];
	}
	
	var lButton;
	if(options.id && document.getElementById(options.id) && document.getElementById(options.id).nodeName.toLowerCase() === "button") {
		if(typeof Ladda !== "undefined" && document.querySelector( '#' + options.id ) ) {
			lButton = Ladda.create( document.querySelector( '#' + options.id ) );
			lButton.start();
		}
	}
	var xmlhttp = getXmlHttp();
	var requestTypeStr = 'GET';
	if(typeof options.requestType !== "undefined" && options.requestType === "POST") {
		requestTypeStr = "POST";
	}
	xmlhttp.open(requestTypeStr, '/cgi-bin/' + options.uri, true);
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4) {
			if(lButton) {
				lButton.stop();
			}
			$('.server-button').attr('disabled', false);
			$('body').css("cursor", "auto");
			if(typeof options.onLoad === "function") {
				options.onLoad();
			}
			if(xmlhttp.status == 200) {
				checkRequest( {
					'responseText'	:	xmlhttp.responseText,
					'id'	:	options.id,
					'message':	 options.messageAbout,
					'checkContent': options.checkContent,
					'callback' : options.callback
				});
			} else if (xmlhttp.status > 0 && xmlhttp.status < 200 && xmlhttp.status >= 300) {
				if(options.needRefreshAfterBadStatus) {
					if(!leavingPage && xmlhttp.status > 0 && xmlhttp.status < 200 && xmlhttp.status >= 300 ){
						if (confirm('Ошибка чтения данных с сервера №' + xmlhttp.status + '. Повторить?')) {
							processRequest(options);
						} else {
							// Do nothing!
						}
					}
				} else {
					alert(options.messageAbout + ' Невозможно обработать http запрос! Код ошибки: ' + xmlhttp.status, 5);
				}
			}
		}
	}
	$('.server-button').attr('disabled', true);
	$('body').css("cursor", "wait");
	xmlhttp.send(options.messageToSend);	
}

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt )
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}