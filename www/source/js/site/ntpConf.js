function createNtpConf() {
	var ntpConfFile = "# \n# NTP configuration file (ntp.conf)\n# \n";
	var protocolIndex = mainObject.protocolTypes.indexOf(mainObject.protocol);
	var baudRateIndex = mainObject.baudRates.indexOf(mainObject.baudrate);
	ntpConfFile += "# Servers / peers section\n";
	if (mainObject.protocol != "NONE") {
		ntpConfFile += "server 127.127." + mainObject.protocolIDs[protocolIndex] + ".0 mode " + (parseInt(mainObject.ntpBauds[baudRateIndex], 10) + parseInt(mainObject.command, 10))
			+ " minpoll " + mainObject.minpoll + " maxpoll " + mainObject.maxpoll 
			+ (mainObject.prefer == "internal" ? " prefer" : "") + "\n";
		
		ntpConfFile += "fudge 127.127." + mainObject.protocolIDs[protocolIndex] + ".0"
		for(var i = 1;  i < 4;  i++)
			if(mainObject["flag" + i])
				ntpConfFile += " flag" + i + " " + mainObject["flag" + i];
		ntpConfFile += " stratum " + (parseInt(mainObject.stratum_internal) - 1);
		if(mainObject.time1)
			ntpConfFile += " time1 " + mainObject.time1;
		ntpConfFile += " refid " + mainObject.refid + "\n";
		ntpConfFile += "\n";
	}
	for(var i = 1;  i <= 4; i++) {
		if(mainObject["server" + i + "_external"]) {
			ntpConfFile += "server " + mainObject["server" + i + "_external"] + " iburst " + (mainObject.prefer == ("server" + i) ? " prefer" : "") + "\n";
		}
	}
	if(typeof mainObject.local_clock !== "undefined" && mainObject.local_clock === "1") {
		ntpConfFile += "server 127.127.1.0\n";
		ntpConfFile += "fudge 127.127.1.0 stratum " + mainObject.stratum_local + " refid LOCAL\n"
	}
	
	ntpConfFile += "\n";
	ntpConfFile += "\n# Access controls section\n";
	if (mainObject.access_type === "restriction") {
		ntpConfFile += 'restrict 127.0.0.1\n';
		ntpConfFile += "restrict default " + mainObject.restrict_flags_default.join(' ') + "\n";
		ntpConfFile += "restrict source " + mainObject.restrict_flags_source.join(' ') + "\n";
		ntpConfFile += "\n";
	
		for(var i = 1;  i <= 4; i++) {
			if(mainObject["server" + i + "_external"]) {
				ntpConfFile += "restrict " + mainObject["server" + i + "_external"] + ' ' + mainObject.restrict_flags_source.join(' ') + "\n"
			}
		}
	
		for(var i = 0;  i < mainObject.restrict_addresses.length;  i++) {
			ntpConfFile += "restrict " + mainObject.restrict_addresses[i];
			if(mainObject.restrict_masks[i] && 
				/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(mainObject.restrict_addresses[i])) {
					ntpConfFile += " mask " +  mainObject.restrict_masks[i];
			}
				ntpConfFile += " " + mainObject["restrict_flags_" + mainObject.restrict_types[i]].join(' ') + "\n";
		}
		ntpConfFile += "\n";
	}
	ntpConfFile += "# Miscellaneous section\n";
	if(mainObject.driftfile) {
		ntpConfFile += "driftfile " + mainObject.driftfile + "\n";
	}
	if(mainObject.logfile) {
		ntpConfFile += "logfile " + mainObject.logfile + "\n";
	}
	
	ntpConfFile += "\n# Statistics section\n";
	
	if(mainObject.statistic_enable == "1") {
		ntpConfFile += "statsdir " + mainObject.statsdir + "\n";
		ntpConfFile += "statistics " + mainObject.statistics.join(' ') + "\n";
		ntpConfFile += "\n";
		for (var i = 0;  i < mainObject.statistics.length;  i++) {
			ntpConfFile += "filegen " + mainObject.statistics[i] + " file " + mainObject.statistics[i] + " type " + mainObject.stats_type + " enable\n";
		}
		ntpConfFile += "\n";
	}
	else {
		ntpConfFile += "disable stats\n";
	}
	return ntpConfFile;
}
		
function uploadNTPconf(onFinish) {
	processRequest({
		'id' : 'saveConfigButton',
		'uri' : 'uploadNTPconf',
		'messageAbout' : 'Загрузка на сервер файла ntp.conf .',
		'requestType'	:	'POST',
		'messageToSend':	createNtpConf(mainObject),
		'callback' : function(responseText) {
			if(typeof clearAllBackgrounds === "function")
				clearAllBackgrounds();
			if(typeof onFinish === "function")
				onFinish();
		}
	});
}
