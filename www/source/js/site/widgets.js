function showPopoverMessage(id, message) {
	$('#' + id).popover({
		'show': true,
		'placement': 'top',
		'trigger': 'none',
		'content': message
	});
	$('#' + id).popover('show');
	setTimeout(function() { $("#" + id).popover("destroy"); }, 3000);
}